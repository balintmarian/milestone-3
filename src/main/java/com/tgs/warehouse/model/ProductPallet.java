package com.tgs.warehouse.model;


import javax.persistence.*;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "product_pallet")
public class ProductPallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "description")
    private String description;
    @OneToMany(targetEntity = ProductPackage.class, fetch = FetchType.LAZY, mappedBy = "productPallet")
    private List<ProductPackage> packages;


    public ProductPallet() {
    }

    public ProductPallet(String description) {
        this.description = description;
    }

    public ProductPallet(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public ProductPallet(String description, List<ProductPackage> packages) {
        this.description = description;
        this.packages = packages;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.toLowerCase();
    }

    public List<ProductPackage> getPackages() {
        return packages;
    }

    public void setPackages(List<ProductPackage> packages) {
        this.packages = packages;
    }

}

