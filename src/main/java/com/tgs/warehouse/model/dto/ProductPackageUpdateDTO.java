package com.tgs.warehouse.model.dto;

import com.tgs.warehouse.model.ProductPackage;

public class ProductPackageUpdateDTO extends AbstractUpdateDTO<ProductPackage> {

    private String type;

    private Long productPalletId;

    public Long getProductPalletId() {
        return productPalletId;
    }

    public void setProductPalletId(Long productPalletId) {
        this.productPalletId = productPalletId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
