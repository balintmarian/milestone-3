package com.tgs.warehouse.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tgs.warehouse.model.ProductPackage;
import com.tgs.warehouse.model.ProductPallet;

import java.util.List;

public class ProductPalletCreationDTO extends AbstractCreationDTO<ProductPallet> {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<ProductPackage> packages;

    public List<ProductPackage> getPackages() {
        return packages;
    }

    public void setPackages(List<ProductPackage> packages) {
        this.packages = packages;
    }
}
