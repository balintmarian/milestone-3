package com.tgs.warehouse.model;

import javax.persistence.*;

@Entity
@Table(name = "product_package")
public class ProductPackage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @ManyToOne
    private ProductPallet productPallet;
    @Column(name = "description")
    private String description;
    @Column(name = "type")
    private String type;

    public ProductPackage() {
    }

    public ProductPackage(String description, String type) {
        this.description = description;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.toLowerCase();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type.toLowerCase();
    }

    public ProductPallet getProductPallet() {
        return productPallet;
    }

    public void setProductPallet(ProductPallet productPallet) {
        this.productPallet = productPallet;
    }
}
