package com.tgs.warehouse;

import com.tgs.warehouse.model.ProductPackage;
import com.tgs.warehouse.model.ProductPallet;
import com.tgs.warehouse.model.dto.ProductPackageCreationDTO;
import com.tgs.warehouse.model.dto.ProductPalletUpdateDTO;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.addMappings(new PropertyMap<ProductPallet, ProductPalletUpdateDTO>() {
            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setDescription(source.getDescription());
            }
        });
        mapper.addMappings(new PropertyMap<ProductPackage, ProductPackageCreationDTO>() {
            @Override
            protected void configure() {
                when(Conditions.isNotNull()).map().setProductPalletId(source.getProductPallet().getId());
            }
        });
        return mapper;
    }
}
