package com.tgs.warehouse.controller;

import com.tgs.warehouse.model.ProductPallet;
import com.tgs.warehouse.model.dto.AbstractCreationDTO;
import com.tgs.warehouse.model.dto.AbstractUpdateDTO;
import com.tgs.warehouse.model.dto.ProductPalletCreationDTO;
import com.tgs.warehouse.model.dto.ProductPalletUpdateDTO;
import com.tgs.warehouse.service.IService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Objects;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/api/pallet")
public class PalletController {

    private final IService<ProductPallet> productPalletService;

    @Autowired
    public PalletController(IService<ProductPallet> productPalletService) {
        this.productPalletService = Objects.requireNonNull(productPalletService);
    }

    @RequestMapping(method = GET)
    public Collection<AbstractUpdateDTO<ProductPallet>> search(@RequestParam(value = "description", required = false) String description) {
        if (!ObjectUtils.allNotNull(description)||ObjectUtils.isEmpty(description.trim())) {
            return productPalletService.findAll();
        }
        return productPalletService.findAllByDescription(description);
    }

    @RequestMapping(method = GET, value = "/{id}")
    public ProductPallet findOne(@PathVariable("id") Long id) {
        return productPalletService.findOne(id);
    }

    @RequestMapping(method = DELETE, value = "/{id}")
    public String deletePallet(@PathVariable("id") Long id) {

        try {
            productPalletService.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException("Delete failed, the id=" + id + " doesnt correspond to any pallet");
        }
        return "Pallet with id:" + id + " has been deleted";
    }



    @RequestMapping(method = POST)
    public AbstractCreationDTO<ProductPallet> insert(@RequestBody ProductPalletCreationDTO productPalletDTO) {

        return productPalletService.save(productPalletDTO);
    }

    @RequestMapping(method = PUT)
    public AbstractUpdateDTO<ProductPallet> update(@RequestBody ProductPalletUpdateDTO productPalletDTO) {

        return productPalletService.update(productPalletDTO);

    }
}


