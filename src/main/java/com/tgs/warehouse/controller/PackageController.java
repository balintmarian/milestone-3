package com.tgs.warehouse.controller;

import com.tgs.warehouse.model.ProductPackage;
import com.tgs.warehouse.model.dto.AbstractCreationDTO;
import com.tgs.warehouse.model.dto.AbstractUpdateDTO;
import com.tgs.warehouse.model.dto.ProductPackageCreationDTO;
import com.tgs.warehouse.model.dto.ProductPackageUpdateDTO;
import com.tgs.warehouse.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Objects;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/api/package")
public class PackageController {

    private final IService<ProductPackage> productPackageIService;

    @Autowired
    public PackageController(IService<ProductPackage> productPackageIService) {
        this.productPackageIService = Objects.requireNonNull(productPackageIService);
    }

    @RequestMapping(method = GET)
    public Collection<AbstractUpdateDTO<ProductPackage>> listAll() {

        return productPackageIService.findAll();
    }

    @RequestMapping(method = POST)
    public AbstractCreationDTO<ProductPackage> insert(@RequestBody ProductPackageCreationDTO productPackageDTO) {

        return productPackageIService.save(productPackageDTO);
    }

    @RequestMapping(method = PUT)
    public AbstractUpdateDTO<ProductPackage> update(@RequestBody ProductPackageUpdateDTO productPackageDTO) {

        return productPackageIService.update(productPackageDTO);
    }

    @RequestMapping(method = DELETE, value = "/{id}")
    public String deletePallet(@PathVariable("id") Long id) {

        try {
            productPackageIService.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException("Delete failed, the id=" + id + " doesnt correspond to any package");
        }
        return "Package with id:" + id + " has been deleted";
    }
}


