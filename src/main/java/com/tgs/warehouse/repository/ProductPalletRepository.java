package com.tgs.warehouse.repository;

import com.tgs.warehouse.model.ProductPallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProductPalletRepository extends JpaRepository<ProductPallet, Long> {


    @Query(value = "(select product_pallet.id, product_pallet.description \n" +
            "from product_pallet where product_pallet.description like %:description%)\n" +
            "UNION ALL\n" +
            "(select product_pallet.id, product_pallet.description \n" +
            "from product_pallet inner join product_package on product_pallet.id =product_package.product_pallet_id  \n" +
            "WHERE product_package.description LIKE %:description% \n" +
            "GROUP BY product_pallet.id)",nativeQuery = true)
    Collection<ProductPallet> findAllByDescription(@Param("description") String description);

    void deleteById(Long id);

}
