var palletId = 0;

function submitProduct() {

    console.log("submiting product");
    var product = {
        description: document.getElementById("productDescription").value,
        type: document.getElementById("productType").value,
        productPalletId:palletId
    };
    // productList.push(product);
    //var data={product:product};

    $("#displayProduct")
        .append("<a>" + "description: " + product.description + " type: " + product.type + "</a>");


    $.ajax({
        url: '/api/package'
        , type: 'POST'
        , contentType: 'application/json'
        , dataType: 'json'
        , data: JSON.stringify(product),
        success: function (response) {

        }
    });
}

function submitPallet() {
    var palletDescription = {description: document.getElementById("palletDescription").value};


    $.ajax({
        url: '/api/pallet'
        , type: 'POST'
        , contentType: 'application/json'
        , dataType: 'json'
        , data: JSON.stringify(palletDescription),
        success: function (response) {
            console.log((response.id) + " <--id");

             $('#submit').attr("disabled", false);
             $('#submitProduct').attr("disabled", true);
             $("#displayProduct").empty();
             $("#footer").text(response + " added");
        }
    });
}


