package com.tgs.warehouse.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tgs.warehouse.Application;
import com.tgs.warehouse.model.ProductPallet;
import com.tgs.warehouse.model.dto.AbstractCreationDTO;
import com.tgs.warehouse.model.dto.AbstractUpdateDTO;
import com.tgs.warehouse.model.dto.ProductPalletCreationDTO;
import com.tgs.warehouse.model.dto.ProductPalletUpdateDTO;
import com.tgs.warehouse.service.IService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
@Transactional
public class PalletControllerTest {

    @Autowired
    PalletController palletController;

    @Autowired
    MockMvc mvc;

    @Autowired
    ModelMapper mapper;

    @Autowired
    IService<ProductPallet> productPalletService;

    @Test()
    public void searchPalletByDescriptionAndWithEmptyDescriptionTest() throws Exception {
        AbstractCreationDTO<ProductPallet> palletCreationDTO = createNewPalletWithDescription();

        List<AbstractUpdateDTO<ProductPallet>> productPalletDTOs = new LinkedList<>();
        AbstractUpdateDTO<ProductPallet> palletUpdateDTO = mapper.map(productPalletService.save(palletCreationDTO), ProductPalletUpdateDTO.class);
        productPalletDTOs.add(palletUpdateDTO);

        String response = mvc.perform(get("/api/pallet/?description={description}", palletUpdateDTO.getDescription()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String jsonObject = new ObjectMapper().writeValueAsString(productPalletDTOs);
        assertThat(response).isEqualTo(jsonObject);

        AbstractUpdateDTO<ProductPallet> auxPalletUpdateDTO = mapper.map(productPalletService.save(palletCreationDTO), ProductPalletUpdateDTO.class);
        productPalletDTOs.add(auxPalletUpdateDTO);
        response = mvc.perform(get("/api/pallet/?description="))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        jsonObject = new ObjectMapper().writeValueAsString(productPalletDTOs);
        assertThat(response).isEqualTo(jsonObject);
    }

    @Test
    public void findOneProductPalletById() throws Exception {
        AbstractCreationDTO<ProductPallet> palletCreationDTO = createNewPalletWithDescription();

        palletCreationDTO = productPalletService.save(palletCreationDTO);

        String response = mvc.perform(get("/api/pallet/{id}", palletCreationDTO.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String jsonObject = new ObjectMapper().writeValueAsString(palletCreationDTO);
        assertThat(response).isEqualTo(jsonObject);
    }

    @Test
    public void deletePalletById() throws Exception {
        AbstractCreationDTO<ProductPallet> palletCreationDTO = createNewPalletWithDescription();

        palletCreationDTO = productPalletService.save(palletCreationDTO);

        String response = mvc.perform(delete("/api/pallet/{id}", palletCreationDTO.getId()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo("Pallet with id:" + palletCreationDTO.getId() + " has been deleted");
    }

    @Test
    public void insertPalletTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        ProductPalletCreationDTO palletCreationDTO = createNewPalletWithDescription();

        String jsonObject = objectMapper.writeValueAsString(palletCreationDTO);

        String response = mvc.perform(post("/api/pallet").contentType(APPLICATION_JSON_UTF8).content(jsonObject))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        palletCreationDTO.setId(objectMapper.readTree(response).get("id").asLong());

        assertThat(response).isEqualTo(objectMapper.writeValueAsString(palletCreationDTO));
    }

    @Test
    public void updatePalletTest() throws Exception {
        String before = "testbeforeupdate";
        String after = "testafterupdate";
        ObjectMapper objectMapper = new ObjectMapper();
        ProductPalletCreationDTO palletCreationDTO = new ProductPalletCreationDTO();
        palletCreationDTO.setDescription(before);

        AbstractUpdateDTO<ProductPallet> palletUpdateDTO = mapper.map(productPalletService.save(palletCreationDTO), ProductPalletUpdateDTO.class);
        palletUpdateDTO.setDescription(after);
        String jsonObject = objectMapper.writeValueAsString(palletUpdateDTO);

        String response = mvc.perform(put("/api/pallet").contentType(APPLICATION_JSON_UTF8).content(jsonObject))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObject);

        ProductPallet ppFromDb = productPalletService.findOne(palletUpdateDTO.getId());
        palletUpdateDTO = mapper.map(ppFromDb, ProductPalletUpdateDTO.class);
        String jsonPalletFromDb = objectMapper.writeValueAsString(palletUpdateDTO);

        assertThat(jsonPalletFromDb).isEqualTo(jsonObject);
    }

    private ProductPalletCreationDTO createNewPalletWithDescription() {
        ProductPalletCreationDTO palletCreationDTO = new ProductPalletCreationDTO();
        palletCreationDTO.setDescription("testdescription");
        return palletCreationDTO;
    }
}

